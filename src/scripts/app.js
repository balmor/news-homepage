const menuElement = document.querySelector('.toggle-menu');
const wrapElement = document.getElementById('wrapper');

menuElement.addEventListener('click', () => {
  wrapElement.classList.toggle('outside');
})