# Frontend Mentor - News homepage solution

This is a solution to the [News homepage challenge on Frontend Mentor](https://www.frontendmentor.io/challenges/news-homepage-H6SWTa1MFl). Frontend Mentor challenges help you improve your coding skills by building realistic projects.

## Table of contents

- [Overview](#overview)
  - [The challenge](#the-challenge)
  - [Screenshot](#screenshot)
  - [Links](#links)
- [My process](#my-process)
  - [Built with](#built-with)
  - [Time spent](#time-spent)
- [Author](#author)

## Overview

### The challenge

Users should be able to:

- View the optimal layout for the interface depending on their device's screen size
- See hover and focus states for all interactive elements on the page

### Screenshot

[<img src="./src/assets/images/news.png" width="500"/>](./src/assets/images/news.png)

### Links

- Solution URL: [News Homepage](https://gitlab.com/balmor/news-homepage)
- Live Site URL: [News Homepage](https://balmor.gitlab.io/news-homepage/)

## My process

### Built with

- Semantic HTML5 markup
- CSS custom properties
- Flexbox
- CSS Grid
- Mobile-first workflow

### Time spent
- 1 day (6h)
- 2 day (6h)

Total hours: 12h

## Author

- Website - [Damian Duda](https://balmor.github.io/)
- Frontend Mentor - [@balmor](https://www.frontendmentor.io/profile/balmor)
